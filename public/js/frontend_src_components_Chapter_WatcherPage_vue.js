"use strict";
(self["webpackChunkexample_app"] = self["webpackChunkexample_app"] || []).push([["frontend_src_components_Chapter_WatcherPage_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[42].use[0]!./frontend/src/components/Chapter/WatcherPage.vue?vue&type=script&lang=js":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[42].use[0]!./frontend/src/components/Chapter/WatcherPage.vue?vue&type=script&lang=js ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  data: function data() {
    return {
      rangeVal: 70
    };
  },
  watch: {
    rangeVal: function rangeVal(val) {
      if (val > 20 && val < 60) {
        if (val < 40) {
          this.rangeVal = 20;
        } else {
          this.rangeVal = 60;
        }
        console.log(this.rangeVal);
      }
    }
  },
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/templateLoader.js??clonedRuleSet-25!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[42].use[0]!./frontend/src/components/Chapter/WatcherPage.vue?vue&type=template&id=1f3c157a":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/templateLoader.js??clonedRuleSet-25!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[42].use[0]!./frontend/src/components/Chapter/WatcherPage.vue?vue&type=template&id=1f3c157a ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./frontend/node_modules/vue/dist/vue.esm-bundler.js");

var _hoisted_1 = {
  id: "app"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: "range",
    min: "0",
    max: "100",
    step: "1",
    "onUpdate:modelValue": _cache[0] || (_cache[0] = function ($event) {
      return $data.rangeVal = $event;
    })
  }, null, 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $data.rangeVal]]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("code", null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($data.rangeVal), 1 /* TEXT */)])])]);
}

/***/ }),

/***/ "./frontend/src/components/Chapter/WatcherPage.vue":
/*!*********************************************************!*\
  !*** ./frontend/src/components/Chapter/WatcherPage.vue ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _WatcherPage_vue_vue_type_template_id_1f3c157a__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./WatcherPage.vue?vue&type=template&id=1f3c157a */ "./frontend/src/components/Chapter/WatcherPage.vue?vue&type=template&id=1f3c157a");
/* harmony import */ var _WatcherPage_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./WatcherPage.vue?vue&type=script&lang=js */ "./frontend/src/components/Chapter/WatcherPage.vue?vue&type=script&lang=js");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_WatcherPage_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_WatcherPage_vue_vue_type_template_id_1f3c157a__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"frontend/src/components/Chapter/WatcherPage.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (__exports__);

/***/ }),

/***/ "./frontend/src/components/Chapter/WatcherPage.vue?vue&type=template&id=1f3c157a":
/*!***************************************************************************************!*\
  !*** ./frontend/src/components/Chapter/WatcherPage.vue?vue&type=template&id=1f3c157a ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_templateLoader_js_clonedRuleSet_25_node_modules_vue_loader_dist_index_js_ruleSet_1_rules_42_use_0_WatcherPage_vue_vue_type_template_id_1f3c157a__WEBPACK_IMPORTED_MODULE_0__.render)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_templateLoader_js_clonedRuleSet_25_node_modules_vue_loader_dist_index_js_ruleSet_1_rules_42_use_0_WatcherPage_vue_vue_type_template_id_1f3c157a__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!../../../../node_modules/vue-loader/dist/templateLoader.js??clonedRuleSet-25!../../../../node_modules/vue-loader/dist/index.js??ruleSet[1].rules[42].use[0]!./WatcherPage.vue?vue&type=template&id=1f3c157a */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/templateLoader.js??clonedRuleSet-25!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[42].use[0]!./frontend/src/components/Chapter/WatcherPage.vue?vue&type=template&id=1f3c157a");


/***/ }),

/***/ "./frontend/src/components/Chapter/WatcherPage.vue?vue&type=script&lang=js":
/*!*********************************************************************************!*\
  !*** ./frontend/src/components/Chapter/WatcherPage.vue?vue&type=script&lang=js ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_1_rules_42_use_0_WatcherPage_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__["default"])
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_1_rules_42_use_0_WatcherPage_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[1].rules[42].use[0]!./WatcherPage.vue?vue&type=script&lang=js */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[42].use[0]!./frontend/src/components/Chapter/WatcherPage.vue?vue&type=script&lang=js");
 

/***/ })

}]);